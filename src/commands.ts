/***********************************************************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 **********************************************************************************************************************/
// Force registration in the commands to run.
import './commands/add_sticker';
import './commands/change_sticker';
import './commands/list_sticker';
import './commands/ping';
import './commands/preview_sticker';
import './commands/remove_sticker';
import './commands/rename_sticker';
import './commands/sticker';